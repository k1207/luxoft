# Kayak.ch Automation
## Dependencies
- This Program is has dependencies configured in build.gradle file and has been exectued with gradle verion 7.3.1
- Right now execution in chrome is supported and has been tested with chrome verion 96.0.4664.110
- Chrome driver has been saved in ./src/main/resources/ folder
- Chrome driver has been saved in ./src/main/resources/ folder

## Execution
Test file **FlightSearch.java** is place under **luxoft\lib\src\test\java\luxoft\kayak** and can be executed as testNG test

## Data File
**Data.json** is a json file which contains following fields
{
  "origin": "Berlin",
  "destination": "Munich",
  "startDate": "28.12.2021",
  "returnDate": "27.12.2021",
  "maxPrice":"180"
}

Note:- dates follow dd.MM.yyyy format

## Custom Exception
Code will raise a **InvalidDateException** when 
- flight start date is less than current date
- start date is less than return date

## Assertion Logic
Asserting that all results below specific price can be done using 2 functions
- assertAllReultsHaveRoundTrip :- This is the faster function and it simply checks result elements are half of child flight elements
- iterateAllReultsAndValidateRoundTrip :- over here we itrate each result element and try to ensure that there are exactly 2 flight elements