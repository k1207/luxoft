package luxoft.kayak;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import utils.calendarNavigation.Calendar;
import utils.calendarNavigation.InvalidDateException;
import utils.dataParser.GetTestData;
import utils.navigation.Navigation;

public class FlightSearch {

	@Test
	public void start() throws FileNotFoundException, ParseException, InterruptedException, InvalidDateException {

		System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		// navigate to website
		driver.get("https://www.kayak.ch/");

		// accept cookie pop up
		WebElement acceptCookie = new WebDriverWait(driver, Duration.ofSeconds(30))
				.until(ExpectedConditions.elementToBeClickable(By.xpath(
						"//div[contains(@id,'cookie-consent-dialog-content')]//button[contains(@id,'accept')]")));
		acceptCookie.click();

		// click on x button to clear existing input in origin city
		driver.findElement(
				By.xpath("//div[@aria-label=\"Eingabe Abflughafen\" and @role=\"textbox\"]//div[@role=\"button\"]"))
				.click();

		// select origin airport
		WebElement originLocInput = new WebDriverWait(driver, Duration.ofSeconds(30))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Von?']")));
		originLocInput.sendKeys(GetTestData.returnTestData().get("origin").toString());

		WebElement firstItemOnList = new WebDriverWait(driver, Duration.ofSeconds(30)).until(ExpectedConditions
				.elementToBeClickable(By.xpath("//div[@role='dialog' and contains(@class,'mod-opened')]//li[1]")));
		firstItemOnList.click();// click on first item in list

		// select destination airport
		driver.findElement(By.xpath("//div[text()='Nach?']")).click();
		WebElement destinationLocInput = new WebDriverWait(driver, Duration.ofSeconds(30))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Nach?']")));
		destinationLocInput.sendKeys(GetTestData.returnTestData().get("destination").toString());
		firstItemOnList = new WebDriverWait(driver, Duration.ofSeconds(30)).until(ExpectedConditions
				.elementToBeClickable(By.xpath("//div[@role='dialog' and contains(@class,'mod-opened')]//li[1]")));
		firstItemOnList.click();// click on first item in list

		// start date selection on calendar
		driver.findElement(By.xpath("(//span[contains(@class,'date') and @role='button'])[1]")).click();
		Calendar.selectDateOnCalendar(driver);

		// click on search button
		driver.findElement(By.xpath("//button[@aria-label='Suchen']")).click();

		// wait for results to load after search
		WebElement loadMoreResults = new WebDriverWait(driver, Duration.ofSeconds(30))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Weitere Ergebnisse')]")));

		// sort by price
		driver.findElement(By.xpath("//span[contains(text(),'Günstigste Option')]")).click();

		// load results till max price is reached
		Navigation.loadMoreResultsTillMaxPrice(driver);
		
		//check all loaded results have round trip
		Navigation.assertAllReultsHaveRoundTrip(driver);
		//Navigation.assertAllReultsHaveRoundTrip(driver);
		driver.quit();
	}
}
