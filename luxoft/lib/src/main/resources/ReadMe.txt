This Program is has dependencies configured in build.gradle file and has been exectued with gradle verion 7.3.1
Right now execution in chrome is supported and has been tested with chrome verion 96.0.4664.110
Test data is accepted from data.json file placed in ./src/main/resources/data/ folder
Chrome driver has been saved in ./src/main/resources/ folder


Test File:- 
	FlightSearch.java can be executed as testNG test

Data File:-
	Data.json is a json file which contains following fields
{
  "origin": "Berlin",
  "destination": "Munich",
  "startDate": "28.12.2021",
  "returnDate": "27.12.2021",
  "maxPrice":"180"
}

Custom Exception:-
	Code will raise a InvalidDateException when 
		a) flight start date is less than current date
		b) start date is less than return date

Assertion Logic:-
	Asserting that all results below specific price can be done using 2 functions
		a)assertAllReultsHaveRoundTrip :- This is the faster function and it simply checks result elements are half of child flight elements
		b)iterateAllReultsAndValidateRoundTrip :- over here we itrate each result element and try to ensure that there are exactly 2 flight elements