package utils.calendarNavigation;

public class InvalidDateException extends Exception
{
    public InvalidDateException(String s)
    {
        // Call constructor of parent Exception
        super(s);
    }
}