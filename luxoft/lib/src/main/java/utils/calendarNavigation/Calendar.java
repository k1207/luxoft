package utils.calendarNavigation;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.dataParser.GetTestData;

public class Calendar {

	public static HashMap<String, String> getDateWithFullMonth() throws FileNotFoundException, ParseException, InvalidDateException {
		String startDate = GetTestData.returnTestData().get("startDate").toString();
		String returnDate = GetTestData.returnTestData().get("returnDate").toString();

		HashMap<String, String> dates = new HashMap<>();

		SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
		Date sDate = oldDateFormat.parse(startDate);
		SimpleDateFormat newDateFormat = new SimpleDateFormat("dd.MMMM.yyyy", Locale.GERMAN);
		Date rDate = oldDateFormat.parse(returnDate);
		validateDates(newDateFormat.format(sDate), newDateFormat.format(rDate));

		dates.put("startDate", newDateFormat.format(sDate));
		dates.put("returnDate", newDateFormat.format(rDate));

		return dates;
	}

	public static void validateDates(String startDate, String returnDate) throws ParseException, InvalidDateException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MMMM.yyyy", Locale.GERMAN);
		Date sdate = sdf.parse(startDate);
		Date rdate = sdf.parse(returnDate);
		Date cdate = new Date();
		if (sdate.after(rdate)) {
	        throw new InvalidDateException("travel start date can't be greater than return date");    
		}
		if (sdate.before(cdate)) {
			throw new InvalidDateException("travel start date can't be less than current date");
		}
	}

	public static void selectDateOnCalendar(WebDriver driver) throws ParseException, FileNotFoundException, InterruptedException, InvalidDateException {
		String defaultMonthOnLeftCalendar = new WebDriverWait(driver, Duration.ofSeconds(30))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[contains(@class,'monthName')])[1]")))
				.getText().replace(" ", ".");
		HashMap<String, String> dates = getDateWithFullMonth();
		int sepPos = dates.get("startDate").indexOf(".");
		String startDate = dates.get("startDate").substring(sepPos + 1);
		
		SimpleDateFormat oldDateFormat = new SimpleDateFormat("MMMM.yyyy", Locale.GERMAN);
		Date sDate = oldDateFormat.parse(startDate);
		Date dDate = oldDateFormat.parse(defaultMonthOnLeftCalendar);
		//select start date
		if (dDate.after(sDate)) {
			while(!dDate.equals(sDate)) {
		        System.out.println("click back "+ dDate +" "+sDate);
				driver.findElement(By.xpath("//div[@aria-label='Vorheriger Monat']")).click();
				Thread.sleep(5000);
				defaultMonthOnLeftCalendar = new WebDriverWait(driver, Duration.ofSeconds(30))
				        .until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[contains(@class,'monthName')])[1]"))).getText().replace(" ", ".");
				dDate = oldDateFormat.parse(defaultMonthOnLeftCalendar);
			}
		} else if (dDate.before(sDate)) {
			while(!dDate.equals(sDate)) {
				System.out.println("click forward");
				driver.findElement(By.xpath("//div[@aria-label='N�chster Monat']")).click();
				Thread.sleep(5000);
				defaultMonthOnLeftCalendar = new WebDriverWait(driver, Duration.ofSeconds(30))
				        .until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[contains(@class,'monthName')])[1]"))).getText().replace(" ", ".");
				dDate = oldDateFormat.parse(defaultMonthOnLeftCalendar);
				if(dDate.equals(sDate)) {
					break;
				}
			}
		} else {
			System.out.println("no action");
		}
		String startDay = dates.get("startDate").split("\\.")[0];
		new WebDriverWait(driver, Duration.ofSeconds(30))
        .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@aria-label=\""+startDay+". "+startDate.replace(".", " ")+"\"]"))).click();
	
		//Select return date
		String defaultMonthOnRightCalendar = new WebDriverWait(driver, Duration.ofSeconds(30))
				.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[contains(@class,'monthName')])[2]")))
				.getText().replace(" ", ".");
		sepPos = dates.get("returnDate").indexOf(".");
		String returnDate = dates.get("returnDate").substring(sepPos + 1);
		Date rDate = oldDateFormat.parse(returnDate);
		dDate = oldDateFormat.parse(defaultMonthOnRightCalendar);
		if (rDate.after(dDate)) {
			while(!dDate.equals(rDate)) {
				System.out.println("click forward");
				driver.findElement(By.xpath("//div[@aria-label='N�chster Monat']")).click();
				Thread.sleep(5000);
				defaultMonthOnRightCalendar = new WebDriverWait(driver, Duration.ofSeconds(30))
				        .until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[contains(@class,'monthName')])[2]"))).getText().replace(" ", ".");
				dDate = oldDateFormat.parse(defaultMonthOnRightCalendar);
			}
		}else {
			System.out.println("no action required");
		}
		String returnDay = dates.get("returnDate").split("\\.")[0];
		new WebDriverWait(driver, Duration.ofSeconds(30))
        .until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@aria-label=\""+returnDay+". "+returnDate.replace(".", " ")+"\"]"))).click();
	}
}