package utils.dataParser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import com.google.gson.Gson;

public class GetTestData {
	public static HashMap<String, String> returnTestData() throws FileNotFoundException{
		    BufferedReader bufferedReader = new BufferedReader(new FileReader("./src/main/resources/data/data.json"));
		    Gson gson = new Gson();
		    HashMap<String, String> json = gson.fromJson(bufferedReader, HashMap.class);
		    return json;
	}
}
