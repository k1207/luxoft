package utils.navigation;

import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import utils.dataParser.GetTestData;

public class Navigation {
		public static void loadMoreResultsTillMaxPrice(WebDriver driver) throws NumberFormatException, FileNotFoundException{
			int maxPrice = Integer.parseInt(GetTestData.returnTestData().get("maxPrice"));
			int lastItemPrice = Integer.parseInt(new WebDriverWait(driver, Duration.ofSeconds(30))
					.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='resultsContainer']//div[@class='multibook-dropdown']//div[@class='above-button']//span[@class='price-text'])[last()]"))).getText().split(" ")[1]);

			do {
				WebElement loadMoreResults = new WebDriverWait(driver, Duration.ofSeconds(30))
						.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Weitere Ergebnisse')]")));
				loadMoreResults.click();
				lastItemPrice = Integer.parseInt(driver.findElement(By.xpath("(//div[@class='resultsContainer']//div[@class='multibook-dropdown']//div[@class='above-button']//span[@class='price-text'])[last()]")).getText().split(" ")[1]);
			}while(lastItemPrice<maxPrice+1);
		}
		
		public static void assertAllReultsHaveRoundTrip(WebDriver driver) throws FileNotFoundException {
			List <WebElement> results = driver.findElements(By.xpath("(//div[@class='resultsContainer']//div[@class='multibook-dropdown']//div[@class='above-button']//span[@class='price-text'])"));
			List <WebElement> flights = driver.findElements(By.xpath("//div[@class='resultWrapper']//ol[@class='flights']/li"));
			Assert.assertEquals(2*results.size(), flights.size());
		}
		
		public static void iterateAllReultsAndValidateRoundTrip(WebDriver driver) throws FileNotFoundException {
			List <WebElement> results = driver.findElements(By.xpath("//div[@class='resultWrapper']"));
			for(WebElement res:results) {
				List <WebElement> resultList = res.findElements(By.tagName("ol"));
				Assert.assertEquals(resultList.size(), 1);
				for (WebElement flight:resultList) {
					List <WebElement> flightList = flight.findElements(By.tagName("li"));
					Assert.assertEquals(flightList.size(), 2);
				}
			}
			List <WebElement> flights = driver.findElements(By.xpath("//div[@class='resultWrapper']//ol[@class='flights']/li"));
			Assert.assertEquals(2*results.size(), flights.size());
		}
}
